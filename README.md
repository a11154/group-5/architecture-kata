# Kata Arquitectura Grupo 5

## Integrantes: 

Alex Yamba,
Kleber Simaliza,
Antonio Insuasti,
Manuel Gomez

## Descripción:

Kata de Arquitectura

## ADR

los ADRs se pueden encontra en 
[ADR](./ADR/)

## Diagrama c4

Los Diagramas C4 se pueden encontrar en [C4](./c4/)

### Diagrama de Contexto:

Existen 3 tipos de usuario los que interactuan con la aplicacion principal, existen 2 sistemas
externos que se conectan con el sistema principal, la pasarela de pagos y el servicio de correos.

![Diagrama_contexto](c4/structurizr-1-SystemContext.png)

### Diagrama de Containers:

Se crearon 3 aplicaciones de usuario final como SPAs las cuales pasan por un Gateway de APIS
el cual redirige las peticiones a los servicios desplegados en la nube Azure los cuales guardan lops
datos en una base principal SQL Server. Los sistemas externos interactuan con los servicios dentro de
Azure Cloud.

![Diagrama_container](c4/structurizr-1-Sistemaderegistroypagos-Container.png)

### Diagrama de Componentes (Azure Cloud):

Los componentes principales estan planteados como microservicios que se comunican usando
HTTP/JSON. Se utilizan componentes del proveedor como el servicio de colas, el scheduler y 
table storage para varias tareas detalladas en cada componente. 

![Diagrama_container](c4/structurizr-1-Sistemaderegistroypagos-AzureCloud-Component.png)


### Leyenda C4:

![Diagrama_container](c4/structurizr-1-SystemContext-key.png)
![Diagrama_container](c4/structurizr-1-Sistemaderegistroypagos-Container-key.png)
![Diagrama_container](c4/structurizr-1-Sistemaderegistroypagos-AzureCloud-Component-key.png)