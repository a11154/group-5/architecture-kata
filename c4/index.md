### Diagramas como Codigo 

Usando structurizr definidimos la arquitectura como codigo

### Notas para ejecutar structurizr lite en docker

Seguir las instrucciones de [https://structurizr.com/help/lite/usage].

Los comandos son:
	docker pull structurizr/lite
	docker run -it --rm -p 8080:8080 -v PATH_LOCAL:/usr/local/structurizr structurizr/lite

En windows: Reemplazar la variable PATH por la ruta completa de la carpeta donde estan los archivos
de fuente, poner la ruta dentro de comillas dobles, ej. "C:\usuarios\yo\diagramas"