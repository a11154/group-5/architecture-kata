workspace "Arquitectura Grupo 5" "Modelo de Sistema de registro y pagos educacion." {

    model {
        user = person "Estudiante" "Persona que se registra desde afuera." {
            tags "ElementoExterno"
        }
        admin = person "Administrador" "Administrador del sistema."
        conta = person "Contabilidad" "Departamento de Contabilidad."
        
        extPagos = softwareSystem "Pasarela de pagos" {
            description "Sistema de pagos."
            tags "ElementoExterno"
        }

        extCorreo = softwareSystem "Sistema de Correo SMTP" {
            description "Envio de correo por SMTP."
            tags "ElementoExterno"
        }

        cobrosPagos = softwareSystem "Sistema de registro y pagos" {
            description "Aplicacion principal de registro de estudiantes y pagos."
            
            db = container "SQL Server DB" {
                description "Base de datos principal (registros/pagos)"
                tags "Database"
            }

            azureCloud = container "Azure Cloud" {
                description "Servicios de la nube Azure de M$"

                comColas = component "Service Bus" {
                    description "Colas asincronicas Azure"
                    tags "Microservicio"
                }
                comPagos = component "Pagos" {
                    description "Microservicio gestion de pagos"
                    technology "Java 11 - Spring Boot"
                    tags "Microservicio"
                }

                comReportes = component "Reportes" {
                    description "Microservicio generacion de reportes"
                    technology "Python 3 - Flask"
                    tags "Microservicio"
                }

                dbAzure = component "Azure Table Storage" {
                    description "Persistencia notificaciones y logs"
                    tags "Database"
                }

                comNotificacion = component "Notificacion" {
                    description "Microservicio envio de emails y/o sms"
                    technology "Node 14 - Express"
                    this -> dbAzure "Use"
                    this -> extCorreo "Envia correo"
                    tags "Microservicio"
                }

                comScheduler = component "Scheduler Bus" {
                    description "Tareas programadas sincronizacion con registro central"
                    this -> db "Lee y escribe a"
                }

                comColas -> extPagos "Usa"
                comColas -> db "Lee y escribe a"
                comPagos -> db "Lee y escribe a"
                comReportes -> db "Lee y escribe a"

                # otra forma, relacion escriba fuera del elemento
                #comScheduler -> db "Lee y escribe a"
            }

            spa1 = container "SPA Usuarios" {
                description "Javascript, Vue.js"
                tags "SPAS"
            }
            spa2 = container "SPA Admin" {
                description "Javascript, Vue.js"
                tags "SPAS"
            }
            spa3 = container "SPA Contabilidad" {
                description "Javascript, Vue.js"
                tags "SPAS"
            }

            gateway = container "API Gateway" {
                description "Pasarela de Servicios internos sistema"
            }

            spa1 -> gateway "Uses"
            spa2 -> gateway "Uses"
            spa3 -> gateway "Uses"
            gateway -> azureCloud "Makes API calls"
            azureCloud -> db "Lee y escribe a"

            azureCloud -> extPagos
            azureCloud -> extCorreo
        }

        user -> cobrosPagos "Uses"
        admin -> cobrosPagos "Uses"
        conta -> cobrosPagos "Uses"

        cobrosPagos -> extPagos "Makes API calls"
        cobrosPagos -> extCorreo "Envia correos por"

    }

    views {
        systemContext cobrosPagos "SystemContext" "An example of a System Context diagram." {
            include *
            autoLayout lr
        }

        container cobrosPagos {
            include *
            autoLayout lr
        }

        component azureCloud {
            include *
            autoLayout
        }

        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Container" {
                shape Component
                background #1168bd
                color #ffffff
            }
            element "Component" {
                shape Component
                background #1168bd
                color #ffffff
            }
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
            element "ElementoExterno" {
                background #CCCCCC
                color #ffffff
            }
            element "SPAS" {
                shape WebBrowser
                background #08427b
                color #ffffff
            }
            element "Database" {
                shape Cylinder
                background #08427b
                color #ffffff
            }
             element "Microservicio" {
                shape Hexagon
                background #08427b
                color #ffffff
            }
        }
    }
    
}