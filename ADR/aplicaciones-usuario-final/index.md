# Registro de Decision de Arquitectura

# Titulo

Aplicaciones de usuario final

## Fecha:

2022-05-21

## Contexto

Los perfiles de usuarios definidos tienen requerimientos de seguridad específicos, así como acceso a datos restringidos a ciertos tipos de usuario (ej. Admin no puede ver información de estudiantes).

## Decisión:

Se decidió crear 3 aplicaciones de front-end distintas para cada tipo de usuario por separado.

## Consecuencias

Existirán repositorios distintos por aplicación con la lógica específica de acceso además de compartir core de visualización (colores, fuentes, estilos, conjunto conocido como “design system”). De ser necesario, se puede poner reglas de seguridad a nivel de red y servidor para distintas áreas de la empresa con esta arquitectura.

## Status

Revisión