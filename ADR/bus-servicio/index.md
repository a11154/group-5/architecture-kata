# Registro de Decision de Arquitectura

# Titulo

Uso de Bus de servicios y colas

## Fecha:

2022-05-21

## Contexto

El tiempo de respuesta de la pasarela de pagos para las transacciones requeridas no es constante y tiende a ser lento para los usuarios.

## Decisión:

Para el uso de los servicios de la pasarela de pagos, las llamadas se realizarán a través del servicio Service Bus (colas manejadas).

## Consecuencias

Las interacciones con la funcionalidad de pagos serán mucho más rápidas gracias a la naturaleza asincrónica de la cola, adicionalmente se podría implementar reenvíos y otras funcionalidades.

## Status

Revisión