# Registro de Decision de Arquitectura

# Titulo

Uso de Microsoft Azure

## Fecha:

2022-05-18

## Contexto

La empresa actual tiene un contrato macro con Microsoft y tiene descuento en servicios de la nube Azure incluidos en este.

## Decisión:

Utilizar los componentes administrados por la nube Azure para el desarrollo de esta aplicación (backend)

## Consecuencias

La aplicación aprovechará todos los recursos disponibles de Azure sin incurrir en más costos por compra de software/hardware y mantenimiento.

## Status

Aprobado
