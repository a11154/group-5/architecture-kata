# Registro de Decision de Arquitectura

# Titulo

Integración sistema central

## Fecha:

2022-05-20

## Contexto

El sistema central de registro de estudiantes y clases no es accesible a través de APIs u otros métodos de conexión regulares. Solo se puede acceder por formularios web.


## Decisión:

El componente de integración con este sistema utilizará técnicas de “”Screen Scrapping” para acceder y modificar los datos automáticamente sin intervención del usuario externo a este.

## Consecuencias

El equipo tendrá que realizar un laboratorio y análisis de las páginas web y formularios del sistema central para automatizar las operaciones atómicas necesarias que requieran otros servicios del sistema.

## Status

Revisión
