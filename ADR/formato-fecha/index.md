# Registro de Decision de Arquitectura

# Titulo

Formato de fecha estándar

## Fecha:

2022-05-20

## Contexto

A lo largo del sistema, se utilizará el formato de fechas ISO-8601. 

* Ejemplo: 3 PM, 25 de mayo de 2022 = ‘2022-05-25 15:00:00’


## Decisión:

Utilizar este formato en todos los componentes de visualización y procesamiento de fechas.

## Consecuencias

Para lecturas e ingresos de datos se ahorra el desarrollo de ‘parsers’ de formatos de fechas. Las plataformas manejan nativamente este formato.

## Status

Revisión
