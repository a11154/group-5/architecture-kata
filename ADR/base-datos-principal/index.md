# Registro de Decision de Arquitectura

## Titulo

Base de datos principal

## Fecha:

2022-05-20

## Contexto

El equipo tiene experiencia de implementación con bases de datos relacionales incluyendo reportería.

## Decisión:

Se utilizará el servicio de base de datos administrada SQL Server por la nube (Azure SQL Managed Instance)

## Consecuencias

Los datos de naturaleza transaccional (registro de estudiantes, registro de pagos, inscripciones y otros) se guardarán en tablas de una base de datos relacional.

## Status

Aceptado