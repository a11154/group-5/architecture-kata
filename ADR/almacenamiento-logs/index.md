# Registro de Decision de Arquitectura

## Titulo

Almacenamiento de logs independiente

## Fecha:

2022-05-20

## Contexto

Es necesario almacenar logs de eventos del sistema como accesos de usuarios, notificaciones enviadas, accesos a apis y altas/bajas de registros principales.

## Decisión:

Se utilizará el storage Azure table storage para información de eventos de sistema desacoplando de la información transaccional.

## Consecuencias

El servicio de logging se conectará al storage separado de forma asíncrona manteniendo independencia de la base principal y permitiendo utilizar herramientas específicas para análisis de logs de ser necesario.

## Status

Revisión