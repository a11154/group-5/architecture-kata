# Registro de Decision de Arquitectura

# Titulo

Tecnología de desarrollo front-end

## Fecha:

2022-05-21

## Contexto

El equipo de desarrollo tiene experiencia en Vue.js para desarrollo de front-end.

## Decisión:

Se propone desarrollar el front-end como SPAs utilizando el framework Vue.js como base.

## Consecuencias

Será necesario mantener un tracking de las versiones y avances de la librería principal, así como migraciones y mantenimientos de esta. Se propone hacer una revisión del versionamiento de este framework cada 6 meses.

## Status

Aceptada