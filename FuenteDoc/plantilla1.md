# Registro de Decision de Arquitectura

## Titulo

Titulo

## Fecha:

2022-05-21

## Contexto

What is the issue that we're seeing that is motivating this decision or change?

## Decisión:

What is the change that we're proposing and/or doing?

## Consecuencias

What becomes easier or more difficult to do because of this change?

## Status

blah blah